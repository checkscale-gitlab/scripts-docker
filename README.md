# SINGLETON SD - SCRIPTS - DOCKER

This project contains Linux bash scripts to use with docker.

> The **main repository** is hosted in [gitlab.com/singletonsd/scripts/docker](https://gitlab.com/singletonsd/scripts/docker.git) but it is automaticaly mirrored to [github.com/singletonsd](https://github.com/singletonsd/docker.git), [github.com/patoperpetua](https://github.com/patoperpetua/scripts-docker.git) and to [gitlab.com/patoperpetua](https://gitlab.com/patoperpetua/scripts-docker.git). If you are in the Github page it may occur that is not updated to the last version.

## AVAILABLE SCRIPTS

### DOCKER BUILD IMAGE

Generates all docker images contained in a folder named *docker-images*. The project must have the following structure:

```bash
- docker-images
    - base_name
        - Dockerfile
        - tags
    - another_base_name
        - Dockerfile
        - tags
```

Each folder represent a type of image. The folder name will be base name of the generated folder.

The file tags contains the different tags of the base image used. It must contain one tag per line and finished with an empty line. Example:

```bash
8-alpine
10-alpine
```

You can execute the script by executing the following:

```bash
curl -s https://singletonsd.gitlab.io/scripts/docker/latest/docker_build_image.sh | bash /dev/stdin
```

That script contains the following options:

```bash
First Argument: tag
Secound Argument: name
Third Argument: git commit sha
```

It can be downloaded by:

```bash
curl -o docker_build_image.sh -L https://singletonsd.gitlab.io/scripts/docker/latest/docker_build_image.sh
```

### DOCKER BUILD IMAGE MULTIPLE

You can execute the script by executing the following:

```bash
curl -s https://singletonsd.gitlab.io/scripts/docker/latest/docker_build_image_multiple.sh | bash /dev/stdin
```

It has the following available options:

```bash
-h | --help: display help.
-p | --push: push images after building.
-x | --proxy: use proxy.
-b | --base-name: base name of images.
-c | --commit-sha: sha of commit to attach to image.
-o | --only: the name of the image to create image.
-f | --folder: the name of folder containg image files.
-t | --tag: tag of images.
```

Also, it can be used envieronment variables:

```bash
DOCKER_BUILD_BASE_NAME=
DOCKER_BUILD_TAG=
```

It can be downloaded by:

```bash
curl -o docker_build_image_multiple.sh -L https://singletonsd.gitlab.io/scripts/docker/latest/docker_build_image_multiple.sh
```

## DOWNLOAD

All scripts are available also inside a zip file under [this url](https://singletonsd.gitlab.io/scripts/docker/latest/scripts.zip). Or you can execute the following to download:

```bash
mkdir -p binaries && \
curl -o binaries/scripts.zip -L https://singletonsd.gitlab.io/scripts/docker/latest/scripts.zip && \
cd binaries && unzip scripts.zip && mv src/* . && rm -r src && rm -r scripts.zip && cd ..
```

## STRUCTURE

Master branch is setup as latest folder. To use an specific version, put the version name before the file name like:

```url
https://singletonsd.gitlab.io/scripts/docker/latest/docker_build_image.sh
https://singletonsd.gitlab.io/scripts/docker/develop/docker_build_image.sh
https://singletonsd.gitlab.io/scripts/docker/v0.0.2/docker_build_image.sh
```

## DOCUMENTATION

- [Shellcheck](https://github.com/koalaman/shellcheck)

## TODO

- [X] Fix documentation.
- [X] Add script to download test script from gitlab pages.
- [X] Zip all scripts and put inside pages.

----------------------

© [Singleton SD](http://www.singletonsd.com), Italy, 2019.
